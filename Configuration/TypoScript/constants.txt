########################################
## INCLUDES FOR STAGING && PRODUCTION ##
########################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_ovr_metaseo/Configuration/TypoScript/Constants/Production" extensions="txt">

##############################
#### OVERRIDE FOR STAGING ####
##############################
[globalString = ENV:HTTP_HOST=staging.*]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_ovr_metaseo/Configuration/TypoScript/Constants/Staging" extensions="txt">
[global]

##############################
## OVERRIDE FOR DEVELOPMENT ##
##############################
[globalString = ENV:HTTP_HOST=development.*]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_ovr_metaseo/Configuration/TypoScript/Constants/Development" extensions="txt">
[global]