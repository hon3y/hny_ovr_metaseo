plugin.metaseo {
  
  metaTags {
    useLastUpdate = 0
    useDetectLanguage = 0
    useCanonical = 0
    useCanonical.strict = 0
    useExpire = 0
    
    robotsEnable = 0
    robotsIndex = 0
    robotsFollow = 0
    robotsArchive = 0
    robotsSnippet = 0
    robotsNoImageindex = 1
    robotsNoTranslate = 1
    robotsOdp = 0
    robotsYdir = 0
  }
  
  services {
    enableIfHeaderIsDisabled = 0

    googleAnalytics =
    googleAnalytics.domainName =
    googleAnalytics.anonymizeIp = 0
    googleAnalytics.trackDownloads = 0    
    googleAnalytics.universalAnalytics = 0
  }

}